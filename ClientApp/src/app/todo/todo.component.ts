import {Component, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToDoModel} from './models/todo.model';
import { Router } from '@angular/router';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html'
})
export class TodoComponent {
  public list: ToDoModel[];

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: Router) {
    http.get<ToDoModel[]>(baseUrl + 'api/ToDo/List').subscribe(result => {
      this.list = result;
    }, error => console.error(error));
  }
  public create(): void{
    this.router.navigate(['todo-edit']);
  }
  public delete(id: number): void {
    this.http.delete<number>(this.baseUrl + 'api/ToDo/Delete/'+id).subscribe(result => {
        console.log("hello delete");
        this.router.navigate(['todo']);
    });
  }
  public toggleStatus(id: number, status: boolean): void {
    console.log("!HELLO toggle "+id+" "+status);
    this.http.put<number>(this.baseUrl + 'api/ToDo/ToggleStatus/'+id,null).subscribe(result => {
      console.log('HELLO toggle');
      this.router.navigate(['todo']);
    });
  }
  public getColor(item: ToDoModel): string {
    var now = new Date().getTime();
    var date = new Date(item.activityDate);
    var dateTime = new Date(item.activityDate).getTime();
    var hour = date.getHours();
    var lessPerHour = date.setHours(hour-1);

    if (item.isChecked || lessPerHour > now)
       return "green";
    else if ((dateTime > now) && (lessPerHour <= now)) {
      return "yellow";
    }
    return "red"
  }
}
