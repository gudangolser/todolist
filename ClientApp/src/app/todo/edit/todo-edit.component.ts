import {Component, OnInit, Inject, Input} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToDoModel} from '../models/todo.model';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';

@Component({
  selector: 'todo-edit',
  styles: [`
        input.ng-touched.ng-invalid {border:solid red 2px;}
        input.ng-touched.ng-valid {border:solid green 2px;}
    `],
  templateUrl: './todo-edit.component.html'
})
export class TodoEditComponent implements OnInit {
  public id: number;
  public model: ToDoModel;
  public isCreateMode = false;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: ActivatedRoute, private navigator: Router) {
  }
  ngOnInit(): void {
    this.router.params.subscribe(params => {
        this.id = params['id'];
        if(this.id > 0) {
            this.http.get<ToDoModel>(this.baseUrl + 'api/ToDo/Edit/'+this.id).subscribe(result => {
                this.model = result;
            }, error => console.error(error));
        } else {
            this.model = new ToDoModel();
            this.isCreateMode = true;
        }
    });
  }
  public onSubmit(): void {
      if(this.isCreateMode) {
        this.http.post<ToDoModel>(this.baseUrl + 'api/ToDo', this.model).subscribe(result => {
               console.log('HELLO create');
               this.navigator.navigate(['todo']);
            });
      } else {
        this.http.put<ToDoModel>(this.baseUrl + 'api/ToDo/Edit/'+this.id, this.model).subscribe(result => {
               console.log('HELLO edit');
               this.navigator.navigate(['todo']);
        });
      }
  }
}
