﻿export class ToDoModel {
  activity: string;
  activityDate: Date;
  isChecked: boolean;
  id: number;
  description: string;
}