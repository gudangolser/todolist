﻿import {Directive, ElementRef, Renderer2, HostListener} from '@angular/core';
 
@Directive({
    selector: '[bold]'
})
export class BoldDirective{
     
    constructor(private element: ElementRef, private renderer: Renderer2){
         
        this.renderer.setStyle(this.element.nativeElement, "cursor", "pointer");
    }
     
    @HostListener("mouseenter") onMouseEnter() {
        this.setFontWeight("normal");
    }
 
    @HostListener("mouseleave") onMouseLeave() {
        this.setFontWeight("bold");
    }
 
    private setFontWeight(val: string) {
        this.renderer.setStyle(this.element.nativeElement, "font-weight", val);
    }
}