import {Component, Inject} from '@angular/core';
import {LoginModel} from './models/login.model';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'login',
  styles: [`
        input.ng-touched.ng-invalid {border:solid red 2px;}
        input.ng-touched.ng-valid {border:solid green 2px;}
    `],
  templateUrl: './login.component.html'
})
export class LoginComponent {
  public model: LoginModel;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.model = new LoginModel();
  }
  public Login(item: LoginModel) {
    console.info("we are into 1 Login method");
    this.http.put<LoginModel>(this.baseUrl + 'api/Login',this.model).subscribe(result => {
      console.log("we are into Login method");
    });
  }
}
