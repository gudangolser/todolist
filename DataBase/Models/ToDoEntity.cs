﻿using System;
namespace ToDoList.DataBase.Models
{
    public class ToDoEntity
    {
        public string Activity { get; set; }
        public DateTime ActivityDate { get; set; }
        public bool IsChecked { get; set; }
        public string Description { get; set; }

        public long Id { get; set; }
    }
}
