﻿using Microsoft.EntityFrameworkCore;
using ToDoList.DataBase.Models;

namespace ToDoList.DataBase.Contexts
{
    public class DatabaseContext : DbContext
    {
        public DbSet<ToDoEntity> ToDoEntities { get; set; }
        public DbSet<UserEntity> UserEntities { get; set; }
        
        public DatabaseContext()
        {
        }
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=ToDo.db");
        }
    }
}