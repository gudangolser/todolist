﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using ToDoList.DataBase.Contexts;

namespace ToDoList.DataBase
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new DatabaseContext(
                serviceProvider.GetRequiredService<DbContextOptions<DatabaseContext>>()))
            {
                if (context.ToDoEntities.Any())
                {
                    return;   // DB has been seeded
                }

                context.ToDoEntities.AddRange(
                    new Models.ToDoEntity
                    {
                        Activity = "wake up",
                        ActivityDate = new DateTime(2018, 1, 13, 6, 0, 0),
                        IsChecked = true
                    },
                    new Models.ToDoEntity
                    {
                        Activity = "drink beer",
                        ActivityDate = new DateTime(2018, 1, 13, 6, 15, 0),
                        IsChecked = false
                    },
                    new Models.ToDoEntity
                    {
                        Activity = "coding",
                        ActivityDate = DateTime.UtcNow,
                        IsChecked = true
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
