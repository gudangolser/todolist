﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using SQLitePCL;
using ToDoList.DataBase.Contexts;
using ToDoList.Models;
using ToDoList.Services;

namespace ToDoList.Controllers
{
    [Route("api/[controller]")]
    public class ToDoController : Controller
    {
        private ToDoService toDoService;
        private DatabaseContext db;

        public ToDoController(DatabaseContext context)
        {
            db = context;
        }
        
        [HttpGet("[action]")]
        public IEnumerable<ToDoModel> List()
        {
            if (toDoService == null)
                toDoService = new ToDoService(db);
            
            return toDoService.GetToDoList();
        }
        [HttpGet("[action]/{id}")]
        public ToDoModel Edit(long id)
        {
            if (toDoService == null)
                toDoService = new ToDoService(db);

            return toDoService.Get(id); 
        }
        [HttpPut("[action]/{id}")]
        public long Edit([FromQuery] long id, [FromBody] ToDoModel model)
        {
            if (toDoService == null)
                toDoService = new ToDoService(db);

             toDoService.Update(model);
            return model.Id;
        }
        [HttpPut("[action]/{id}")]
        public void ToggleStatus(long id)
        {
            if (toDoService == null)
                toDoService = new ToDoService(db);

            toDoService.ToggleStatus(id);
        }
        [HttpPost]
        public long Create([FromBody] ToDoModel model)
        {
            if (toDoService == null)
                toDoService = new ToDoService(db);

            return toDoService.Create(model);
        }
        [HttpDelete("[action]/{id}")]
        public void Delete(long id)
        {
            if (toDoService == null)
                toDoService = new ToDoService(db);

            toDoService.Delete(id);
        }
    }
}
