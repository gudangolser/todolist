﻿namespace ToDoList.Utils
{
    public class ActionResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public object Model { get; set; }
        
    }
}