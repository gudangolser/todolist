﻿using System.Collections.Generic;
using ToDoList.Models;
using ToDoList.DataBase.Contexts;
using ToDoList.DataBase.Models;
using System.Linq;
using System;

namespace ToDoList.Services
{
    public class ToDoService
    {
        private List<ToDoModel> toDoModels;
        private DatabaseContext db;

        public ToDoService(DatabaseContext context)
        {
            db = context;
        }
        
        public List<ToDoModel> GetToDoList()
        {
            if (toDoModels != null)
                return toDoModels;
            
            toDoModels = Load().Select(x=>new ToDoModel{
                Id = x.Id,
                Activity = x.Activity,
                ActivityDate = x.ActivityDate,
                IsChecked = x.IsChecked,
                Description = x.Description                             
            }).ToList();
            return toDoModels;
        }
        private List<ToDoEntity> Load()
        {
            var list = new List<ToDoEntity>();
            foreach (var toDo in db.ToDoEntities)
            {
                list.Add(toDo);
            }            
            return list;
        }
        public ToDoModel Get(long id)
        {
            var model = new ToDoModel();
            var toDoEntity = db.ToDoEntities.FirstOrDefault(x => x.Id == id);
            if (toDoEntity != null)
            {
                model = new ToDoModel
                {
                    Id = toDoEntity.Id,
                    Activity = toDoEntity.Activity,
                    ActivityDate = toDoEntity.ActivityDate,
                    Description = toDoEntity.Description,
                    IsChecked = toDoEntity.IsChecked
                };

            }            
            if (model.Id == 0)
                throw new NullReferenceException($"ToDo with Id {id} not found");
            return model;
        }
        public void Update(ToDoModel model)
        {
            if(model.Id == 0)
                throw new NullReferenceException($"This ToDo not found");
            var toDoEntity = db.ToDoEntities.FirstOrDefault(x => x.Id == model.Id);
            if (toDoEntity != null)
            {
                toDoEntity.Id = model.Id;
                toDoEntity.Activity = model.Activity;
                toDoEntity.ActivityDate = model.ActivityDate;
                toDoEntity.Description = model.Description;
                toDoEntity.IsChecked = model.IsChecked;
            }
            db.SaveChanges();
            
        }
        public void Delete(long id)
        {
            var toDoEntity = db.ToDoEntities.FirstOrDefault(x => x.Id == id);
            if (toDoEntity != null)
            {
                db.ToDoEntities.Remove(toDoEntity);
                db.SaveChanges();
            }
            else
            {
                throw new NullReferenceException($"ToDo with Id {id} not found");
            }
            
        }
        public long Create(ToDoModel model)
        {
            long id;
            var toDoEntity = new ToDoEntity();
            toDoEntity.Activity = model.Activity;
            toDoEntity.ActivityDate = model.ActivityDate;
            toDoEntity.Description = model.Description;

            db.ToDoEntities.Add(toDoEntity);
            db.SaveChanges();
            id = toDoEntity.Id;
            
            return id;
        }

        public void ToggleStatus(long id)
        {            
            var toDoEntity = db.ToDoEntities.FirstOrDefault(x => x.Id == id);
            if (toDoEntity != null)
            {
                toDoEntity.IsChecked = !toDoEntity.IsChecked;
                db.SaveChanges();
            }
            else
            {
                throw new NullReferenceException($"ToDo with Id {id} not found");
            }            
        }
    }
}
