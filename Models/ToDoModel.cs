﻿using System;

namespace ToDoList.Models
{
    public class ToDoModel
    {
        public long Id { get; set; }
        public string Activity { get; set; }
        public DateTime ActivityDate { get; set; }
        public bool IsChecked { get; set; }
        public string Description { get; set; }
    }
}
